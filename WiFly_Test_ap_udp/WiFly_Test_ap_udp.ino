/*
 * Use this sketch to configure the RN171 via CoolTerm.
 * - Upload this sketch to your board (equipped with the seedstudio RN171 shield).
 * - Do not forget to set the jumpers on the RN171 board according to your configuration
 *   of the WiFlySerial instance in this sketch!
 * - Open CoolTerm and connect to your board (check that the baudrate
 *   of this sketch match the RN171's baudrate and CoolTerm's baudrate).
 * - Enter command mode via sending $$$ via CoolTerm (Connection -> Send String)
 *   without a carriage return. The board should reply with CMD.
 * - send 'get everything' with a carriage return to let the board print
 *   the complete configuration of the RN171.
 */

#include <Arduino.h>
#include <Streaming.h>
#include <SoftwareSerial.h>
#include "WiFlySerial.h"
#include "MemoryFree.h"
#include "Credentials.h"


// Pins are 2 for INCOMING TO Arduino, 5 for OUTGOING TO Wifly
// Arduino       WiFly
//  2 - receive  TX   (Send from Wifly, Receive to Arduino)
//  5 - send     RX   (Send from Arduino, Receive to WiFly) 

#define WIFLY_RX_PIN 2
#define WIFLY_TX_PIN 5

WiFlySerial WiFly(WIFLY_RX_PIN, WIFLY_TX_PIN); 

#define REQUEST_BUFFER_SIZE 120
#define HEADER_BUFFER_SIZE 150 
#define BODY_BUFFER_SIZE 100

char bufRequest[REQUEST_BUFFER_SIZE];
char bufHeader[HEADER_BUFFER_SIZE];
char bufBody[BODY_BUFFER_SIZE];


void setup() {
  
  Serial.begin(9600);
  Serial.println(F("Starting WiFly Tester." ) );
  Serial << F("Free memory:") << freeMemory() << endl;  

  WiFly.begin();
  Serial << F("Starting WiFly...") <<  WiFly.getLibraryVersion(bufRequest, REQUEST_BUFFER_SIZE)
   << F(" Free memory:") << freeMemory() << endl;
  
  // get MAC
  Serial << F("MAC: ") << WiFly.getMAC(bufRequest, REQUEST_BUFFER_SIZE) << endl;
  // is connected ?
  
  // WiFly.setDebugChannel( (Print*) &Serial);
  
  /*
  WiFly.setAuthMode( WIFLY_AUTH_WPA2_PSK);
  WiFly.setJoinMode(  WIFLY_JOIN_AUTO );
  WiFly.setDHCPMode( WIFLY_DHCP_ON );

   
  // if not connected restart link
  WiFly.getDeviceStatus();
  if (! WiFly.isifUp() ) {
    Serial << "Leave:" <<  ssid << WiFly.leave() << endl;
    // join
    if (WiFly.setSSID(ssid) ) {    
      Serial << "SSID Set :" << ssid << endl;
    }
    if (WiFly.setPassphrase(passphrase)) {
      Serial << "Passphrase Set :" << endl;
    }
    Serial << "Joining... :"<< ssid << endl;

    if ( WiFly.join() ) {
      Serial << F("Joined ") << ssid << F(" successfully.") << endl;
      WiFly.setNTP( ntp_server ); // use your favorite NTP server
    } else {
      Serial << F("Join to ") << ssid << F(" failed.") << endl;
    }
  } // if not connected

  Serial << F("IP: ") << WiFly.getIP(bufRequest, REQUEST_BUFFER_SIZE) << endl <<
    F("Netmask: ") << WiFly.getNetMask(bufRequest, REQUEST_BUFFER_SIZE) << endl <<
    F("Gateway: ") << WiFly.getGateway(bufRequest, REQUEST_BUFFER_SIZE) << endl <<
    F("DNS: ") << WiFly.getDNS(bufRequest, REQUEST_BUFFER_SIZE) << endl 
    << F("WiFly Sensors: ") << bufBody <<  WiFly.SendCommand("show q 0x177 ",">", bufBody, BODY_BUFFER_SIZE) << endl
    << F("WiFly Temp: ") <<  WiFly.SendCommand("show q t ",">", bufBody, BODY_BUFFER_SIZE)
    << F("WiFly battery: ") << WiFly.getBattery(bufBody, BODY_BUFFER_SIZE) << endl;

  WiFly.SendCommand("set comm remote 0",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);

  WiFly.closeConnection();
  Serial << F("After Setup mem:") << freeMemory() << endl ;
  
  Serial << F("WiFly now listening for commands.  Type 'exit' to listen for wifi traffic.  $$$ (no CR) for command-mode.") << endl;
*/

  WiFly.SendCommand("apmode dublight 7",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);
  
  WiFly.SendCommand("set ip proto 1",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);
  
  WiFly.SendCommand("set ip host 0.0.0.0",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);
  
  /*
  WiFly.SendCommand("set ip remote 6789",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);
  */
  
  
  WiFly.SendCommand("set ip flags 0x40",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);
  

  //leave command mode
  WiFly.SendCommand("exit",">", bufBody, BODY_BUFFER_SIZE);
  memset (bufBody,'\0',BODY_BUFFER_SIZE);

  // clear out prior requests.
  WiFly.flush();
  while (WiFly.available() )
    WiFly.read();
  
}

char chOut;
void loop() {
  // Terminal routine

  // Always display a response uninterrupted by typing
  // but note that this makes the terminal unresponsive
  // while a response is being received.
  
  while(WiFly.available() > 0) {

    Serial.write(WiFly.read());
  }
  
  if(Serial.available()) { // Outgoing data
    WiFly.write( (chOut = Serial.read()) );
    Serial.write (chOut);
  }

} //loop
