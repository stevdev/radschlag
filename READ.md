# Firmware upgrade

1. Enable internet sharing of your Mac in the *System Preferences -> Sharing*. Share your connection from *Ethernet* to computers using *Wi-Fi*. In *Wi-Fo Options…* choose *None* as *Security*. Your Mac should open a WiFi network.
2. Join this network with your RN171
	1. Upload the arduino sketch *WiFly_Test* to your arduino.
	2. Open CoolTerm and connect to your arduino
	3. Enter command mode and configure the RN171 to enable it to join your Mac's WiFi network (set wlan ssid, set wlan channel, set wlan join 0, save, reboot, enter command mode, join)
3. Perform the upgarde as described in section 4.5 of RN171 User's guide
	1. 	Configure FTP (see table 4-2)
	2.  Follow 4.5.2 1. (2. Step did not work for upgrade to version 4.0)